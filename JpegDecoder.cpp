#include "JpegDecoder.h"
#include "picojpeg.h"

#include "application.h"
#include "Config.h"

JpegDecoder::JpegDecoder(File jpeg_video) {
  Load(jpeg_video);
}

JpegDecoder::~JpegDecoder() {
  image_file.close();
}


bool JpegDecoder::Load(File jpeg_video) {
  if (!jpeg_video) return false;

  image_file = jpeg_video;
  image_file.seek(0);
  mcu_y = 0;
  row_y = 0;

  uint8_t status = pjpeg_decode_init(&image_info, pjpeg_need_bytes_callback, this, 0);
  if (status) {
    Serial.println("pjpeg_decode_init() failed");
    Serial.println(status);
    if (status == PJPG_UNSUPPORTED_MODE) {
      Serial.println("Progressive JPEG files are not supported.");
    }

    return false;
  }

  /*
  uint decoded_width, decoded_height;
  uint row_blocks_per_mcu, col_blocks_per_mcu;

  decoded_width = image_info.m_width;
  decoded_height = image_info.m_height;
  uint row_pitch = decoded_width * image_info.m_comps;
  row_blocks_per_mcu = image_info.m_MCUWidth >> 3;
  col_blocks_per_mcu = image_info.m_MCUHeight >> 3;

  Serial.println(decoded_width);
  Serial.println(decoded_height);
  Serial.println(row_pitch);
  Serial.println(image_info.m_MCUWidth);
  Serial.println(image_info.m_MCUHeight);
  Serial.println(image_info.m_MCUSPerRow);
  Serial.println(image_info.m_MCUSPerCol);
  Serial.println(row_blocks_per_mcu);
  Serial.println(col_blocks_per_mcu);*/
  Serial.println("Loaded successfully!");
  return true;
}

bool JpegDecoder::Reload() {
  return Load(image_file);
}

bool JpegDecoder::ReadFrame(uint8_t* buf, uint16_t buflen) {
  if (!image_file) return false;
  if (!rows_decoded || row_y >= image_info.m_MCUHeight ||
      (((mcu_y - 1) * image_info.m_MCUHeight + row_y) >= image_info.m_height)) {
    //Serial.print("MCU Y: ");
    //Serial.println(mcu_y);
    if (!DecodeNext()) {
      return false;
    };
  }
  int start_idx = row_y * FRAME_BYTES;
  memcpy(buf, img_buf + start_idx, min(buflen, FRAME_BYTES));
  row_y++;
  return true;
}

bool JpegDecoder::DecodeNext() {
  int y, x;
  uint8_t *pDst_row;
  uint8_t status;
  uint decoded_width, decoded_height;
  uint row_pitch;

  decoded_width = image_info.m_width;
  decoded_height = image_info.m_height;
  row_pitch = decoded_width * image_info.m_comps;

  rows_decoded = false;

  // Decode a whole row of MCUs.
  int mcu_x = 0;
  while (mcu_x < image_info.m_MCUSPerRow) {
    status = pjpeg_decode_mcu();

    if (status) {
       if (status == PJPG_NO_MORE_BLOCKS) {
         Serial.println("No more blocks to read!");
         return Reload() && DecodeNext();
       } else {
          Serial.println("pjpeg_decode_mcu() failed with status");
          Serial.println(status);
          return false;
       }
    }

    if (mcu_y >= image_info.m_MCUSPerCol) {
      // How the hell did we reach this?
      Serial.println("How did we reach this!!");
      return Reload() && DecodeNext();
    }


    // Copy MCU's pixel blocks into the destination bitmap.
    pDst_row = img_buf + (mcu_x * image_info.m_MCUWidth * image_info.m_comps);
//    pDst_row = img_buf + (mcu_y * image_info.m_MCUHeight) * row_pitch + (mcu_x * image_info.m_MCUWidth * image_info.m_comps);

    for (y = 0; y < image_info.m_MCUHeight; y += 8) {
      const int by_limit = min(8, image_info.m_height - (mcu_y * image_info.m_MCUHeight + y));

      for (x = 0; x < image_info.m_MCUWidth; x += 8) {
         uint8_t *pDst_block = pDst_row + x * image_info.m_comps;

         // Compute source byte offset of the block in the decoder's MCU buffer.
         uint src_ofs = (x * 8U) + (y * 16U);
         const uint8_t *pSrcR = image_info.m_pMCUBufR + src_ofs;
         const uint8_t *pSrcG = image_info.m_pMCUBufG + src_ofs;
         const uint8_t *pSrcB = image_info.m_pMCUBufB + src_ofs;

         const int bx_limit = min(8, image_info.m_width - (mcu_x * image_info.m_MCUWidth + x));

         if (image_info.m_scanType == PJPG_GRAYSCALE) {
            int bx, by;
            for (by = 0; by < by_limit; by++)
            {
               uint8_t *pDst = pDst_block;

               for (bx = 0; bx < bx_limit; bx++)
                  *pDst++ = *pSrcR++;

               pSrcR += (8 - bx_limit);

               pDst_block += row_pitch;
            }
         } else {
            int bx, by;
            for (by = 0; by < by_limit; by++)
            {
               uint8_t *pDst = pDst_block;

               for (bx = 0; bx < bx_limit; bx++)
               {
                  pDst[0] = *pSrcR++;
                  pDst[1] = *pSrcG++;
                  pDst[2] = *pSrcB++;
                  pDst += 3;
               }

               pSrcR += (8 - bx_limit);
               pSrcG += (8 - bx_limit);
               pSrcB += (8 - bx_limit);

               pDst_block += row_pitch;
            }
         }
      }

      pDst_row += (row_pitch * 8);
    }

    mcu_x++;
  }

  if (mcu_x == image_info.m_MCUSPerRow) {
    mcu_y++;
    row_y = 0;
  }


  /*for (int a = 0; a < IMG_X_RES*3; a += 3) {
    Serial.print(img_buf[a]);
    Serial.print(",");
    Serial.print(img_buf[a+1]);
    Serial.print(",");
    Serial.println(img_buf[a+2]);
  }*/
  rows_decoded = true;
  return true;
}

// Callback needed by the picojpeg decoder.
unsigned char JpegDecoder::PjpegCallback(
  unsigned char* buf, unsigned char buf_size, unsigned char *bytes_actually_read) {

  uint n = min(image_file.size() - image_file.position(), buf_size);
  if (n && (image_file.read(buf, n) != n))
    return PJPG_STREAM_READ_ERROR;

  *bytes_actually_read = (unsigned char)(n);
  return 0;
}

static unsigned char pjpeg_need_bytes_callback(unsigned char* buf, unsigned char buf_size, unsigned char *bytes_actually_read, void* this_pointer) {
    JpegDecoder* self = static_cast<JpegDecoder*>(this_pointer);
    return self->PjpegCallback(buf, buf_size, bytes_actually_read);
}
