#ifndef LAMP_H_
#define LAMP_H_

#include "application.h"

class Lamp {
public:
  virtual void display(uint8_t* buf, uint16_t buf_len) = 0;
  virtual void clear_display() = 0;
  virtual ~Lamp() {};

  void br_set(uint8_t br) {
    target_brightness = max(0, min(br, 255));
  }

  void br_down(int delta = 30) {
    target_brightness = brightness - delta;
    if (target_brightness < 50) target_brightness = 50;
  }

  void br_up(int delta = 30) {
    target_brightness = brightness + delta;
    if (target_brightness > 255) target_brightness = 255;
  }

protected:
  int brightness = 255;
  int target_brightness = brightness;
};

#endif  // LAMP_H_
