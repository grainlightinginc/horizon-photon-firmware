#include "HorizonLamp.h"

HorizonLamp::HorizonLamp(uint16_t frame_pixels_, uint8_t top_pin, uint8_t bot_pin) {
  frame_pixels = frame_pixels_;
  strip_pixels = frame_pixels * 2;
  top_strip = new Adafruit_NeoPixel(strip_pixels, top_pin, WS2812B);
  bottom_strip = new Adafruit_NeoPixel(strip_pixels, bot_pin, WS2812B);
}

HorizonLamp::~HorizonLamp() {
  delete top_strip;
  delete bottom_strip;
}

void HorizonLamp::display(uint8_t* buf, uint16_t buf_len) {
    for (int i = 0; i < frame_pixels / 2; ++i) {
      int base = (frame_pixels/2 - i - 1) * 3;
      byte r = 0, g = 0, b = 0;
      // Read out only the pixels sent in packet.
      if (i < buf_len / 3) {
        r = (buf[base] * brightness / 255) % 256;
        g = (buf[base+1] * brightness / 255) % 256;
        b = ((buf[base+2] / 2)  * brightness / 255) % 256;
      }

      int actual_idx = i * 2;
      // TOP strip.
      top_strip->setPixelColor(actual_idx, r, g, b);
      top_strip->setPixelColor(actual_idx + 1, r, g, b);
      top_strip->setPixelColor(strip_pixels - actual_idx - 1, r, g, b);
      top_strip->setPixelColor(strip_pixels - actual_idx - 2, 0, 0, 0);
    }
    for (int i = 0; i < frame_pixels / 2; ++i) {
      int base = (frame_pixels - i - 1) * 3;
      byte r = 0, g = 0, b = 0;
      // Read out only the pixels sent in packet.
      if (i < buf_len / 3) {
        r = (buf[base] * brightness / 255) % 256;
        g = (buf[base+1] * brightness / 255) % 256;
        b = ((buf[base+2] / 2)  * brightness / 255) % 256;
      }

      int actual_idx = i * 2;
      // BOTTOM strip.
      bottom_strip->setPixelColor(actual_idx, r, g, b);
      bottom_strip->setPixelColor(actual_idx + 1, 0, 0, 0);
      bottom_strip->setPixelColor(strip_pixels - actual_idx - 1, r, g, b);
      bottom_strip->setPixelColor(strip_pixels - actual_idx - 2, r, g, b);
    }
    top_strip->show();
    bottom_strip->show();

    if (brightness != target_brightness) {
      int delta = (target_brightness - brightness) / 8;
      if (target_brightness > brightness) {
        delta = max(delta, 1);
      } else {
        delta = min(delta, -1);
      }
      brightness += delta;
    }
}

void HorizonLamp::clear_display() {
  for (int i = 0; i < strip_pixels; i++) {
      byte amount = 10;
      top_strip->setPixelColor(i, amount, 0, 0);
      bottom_strip->setPixelColor(i, 0, amount, 0);
  }
  top_strip->show();
  bottom_strip->show();
}
