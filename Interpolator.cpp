#include "application.h"
#include "Interpolator.h"

Interpolator::Interpolator(VideoStream* video_, uint8_t n_frames, uint8_t step_size_) : video(video_) {
  current_frame = frame_a;
  next_frame = frame_b;
  interpolation_pos = 0;
  interpolation_frames = n_frames;
  step_size = step_size_;
  video->ReadFrame(current_frame, FRAME_BYTES);
  video->ReadFrame(next_frame, FRAME_BYTES);
}

bool Interpolator::ReadFrame(uint8_t* buf, uint16_t buflen) {
  buflen = min(buflen, FRAME_BYTES);
  if (interpolation_pos == 0) {
    // Base case: Simply read out the current frame.
    memcpy(buf, current_frame, buflen);
  } else {
    // We need to interpolate.
    uint32_t dst_mix = (interpolation_pos << 16) / (interpolation_frames + 1);
    uint32_t src_mix = (1 << 16) - dst_mix;
    int i;
    for (i = 0; i < buflen; i++) {
      buf[i] = (byte) ((src_mix * current_frame[i]
              + dst_mix * next_frame[i]) >> 16);
    }
  }
  interpolation_pos += step_size;
  while (interpolation_pos > interpolation_frames) {
    // We're done interpolating between these frames, so swap buffers
    // and read the next frame out.
    byte* tmp = current_frame;
    current_frame = next_frame;
    next_frame = tmp;
    interpolation_pos -= (interpolation_frames + 1);
    if (!video->ReadFrame(next_frame, FRAME_BYTES)) {
      return false;
    }
  }
  return true;
}
