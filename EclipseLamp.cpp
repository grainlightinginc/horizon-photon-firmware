#include "EclipseLamp.h"
#include "owl_common.h"

EclipseLamp::EclipseLamp(uint16_t frame_pixels_, uint8_t pin) {
  frame_pixels = frame_pixels_;
  strip_pixels = frame_pixels * 2;
  strip = new Adafruit_NeoPixel(strip_pixels, pin, WS2812B);
}

EclipseLamp::~EclipseLamp() {
  delete strip;
}

void EclipseLamp::display(uint8_t* buf, uint16_t buf_len) {
  // Alternating pixels.
  for (int i = 0; i < frame_pixels; ++i) {
    // Rotate the data.
      int strip_pixel = mod((i * 2) + 33, strip_pixels);
      int buf_base = i * 3;
      if (buf_base < buf_len) {
          int r = (buf[buf_base] * brightness / 255) % 256;
          int g = (buf[buf_base+1] * brightness / 255) % 256;
          int b = ((buf[buf_base+2] / 2)  * brightness / 255) % 256;
          strip->setPixelColor(strip_pixel, r, g, b);
      } else {
          strip->setPixelColor(strip_pixel, 0, 0, 0);
      }
  }
  // In-betweens.
  for (int i = 0; i < strip_pixels; i += 2) {
      uint32_t c1 = strip->getPixelColor(mod(i + 1, strip_pixels));
      uint32_t c2 = strip->getPixelColor(mod(i - 1, strip_pixels));

      byte r = ( ((c1 >> 16) & 0xFF) / 2 ) + ((c2 >> 16) & 0xFF) / 2;
      byte g = ( ((c1 >>  8) & 0xFF) / 2 ) + ((c2 >>  8) & 0xFF) / 2;
      byte b = ( ((c1 >>  0) & 0xFF) / 2 ) + ((c2 >>  0) & 0xFF) / 2;

     strip->setPixelColor(i, r, g, b);
  }
  strip->show();

  if (brightness != target_brightness) {
    int delta = (target_brightness - brightness) / 8;
    if (target_brightness > brightness) {
      delta = max(delta, 1);
    } else {
      delta = min(delta, -1);
    }
    brightness += delta;
  }
}

void EclipseLamp::clear_display() {
  for (int i = 0; i < strip_pixels; i++) {
      byte amount = 5;
      strip->setPixelColor(i, amount, 0, 0);
  }
  strip->show();
}
