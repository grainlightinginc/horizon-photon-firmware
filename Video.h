#ifndef VIDEO_H_
#define VIDEO_H_

class VideoStream;
class File;

typedef enum {
  GVF,
  JPEG
} FileType;

VideoStream* load_video(File f, FileType t);

#endif // VIDEO_H_
