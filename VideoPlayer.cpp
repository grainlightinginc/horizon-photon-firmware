#include "application.h"

#include "VideoPlayer.h"
#include "VideoStream.h"
#include "Blender.h"
#include "SD.h"
#include "owl_common.h"

VideoPlayer::VideoPlayer(const char* dirname, FileType file_type) {
  vid_type = file_type;
  read_video_names(dirname);
}

void VideoPlayer::read_video_names(const char* dirname) {
  File vid_root = SD.open(dirname);
  if (!vid_root) {
    Serial.println("Open root failed!");
    return;
  }

  num_videos = 0;
  while (num_videos < MAX_VIDEOS) {
    File vid_file = vid_root.openNextFile();

    if (!vid_file) {
        vid_root.rewindDirectory();
        continue;
    }
    if (vid_file.isDirectory()) continue;
    strncpy(video_names[num_videos], dirname, MAX_VIDEO_NAME_LEN);
    strncat(video_names[num_videos], "/", MAX_VIDEO_NAME_LEN);
    strncat(video_names[num_videos], vid_file.name(), MAX_VIDEO_NAME_LEN);
    num_videos++;
  }
}

VideoStream* VideoPlayer::play_video(const char* name, uint32_t duration) {
  File vid_file = SD.open(name);
  if (!vid_file) {
    video = NULL;
    return video;
  }

  VideoStream* new_video = load_video(vid_file, vid_type);
  if (new_video == NULL) {
    Serial.println("Couldn't open video!");
    return false;
  }
  // NOTE: This transfers ownership of video to Blender instance, so we don't
  // need to worry about cleaning up the pointer from load_video.
  new_video = new Blender(video, new_video, duration / 16, FADE);
  if (new_video == NULL) {
    Serial.println("Couldn't blend video!");
    return false;
  }

  strncpy(cur_video_name, name, MAX_VIDEO_NAME_LEN);
  if (Spark.connected()) {
    Spark.publish("video", name);
  }
  video = new_video;
  return new_video;
}

VideoStream* VideoPlayer::prev_video(uint32_t duration) {
  cur_video = mod(cur_video - 1, num_videos);
  return play_video(video_names[cur_video], duration);
}

VideoStream* VideoPlayer::next_video(uint32_t duration) {
  cur_video = mod(cur_video + 1, num_videos);
  return play_video(video_names[cur_video], duration);
}
