#ifndef BLENDER_H_
#define BLENDER_H_

#include <memory>
#include "VideoStream.h"

typedef enum {
  FADE
} BlendType;

class Blender : public VideoStream {
public:
  Blender(VideoStream* source, VideoStream* target, uint16_t n_frames, BlendType type);
  bool ReadFrame(uint8_t* buf, uint16_t buflen);

private:
  uint16_t blend_frames;
  uint8_t blend_pos;
  BlendType blend_type;

  std::auto_ptr<VideoStream> source;
  std::auto_ptr<VideoStream> target;
};

#endif  // BLENDER_H_
