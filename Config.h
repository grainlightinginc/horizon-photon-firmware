#ifndef M_CONFIG_H_
#define M_CONFIG_H_

#include "application.h"

//#define DEREK 1
//#define ECLIPSE 1
#define HORIZON 1
#define SD_CARD 1

#if DEREK
#undef SD_CARD
#endif

#if HORIZON
#include "HorizonConfig.h"
#elif ECLIPSE
#include "EclipseConfig.h"
#else
#error "Must define a lamp model!"
#endif

const uint8_t APDS9960_intPin = D2;
const uint8_t SD_chipSelect = A2;    // Also used for HARDWARE SPI setup
const uint8_t SD_clockPin = A3;
const uint8_t SD_misoPin = A4;
const uint8_t SD_mosiPin = A5;

#endif  // M_CONFIG_H_
