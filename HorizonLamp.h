#ifndef HORIZON_LAMP_H_
#define HORIZON_LAMP_H_

#include "application.h"
#include "neopixel.h"
#include "Lamp.h"

class HorizonLamp : public Lamp {
public:
  HorizonLamp(uint16_t frame_pixels, uint8_t top_pin, uint8_t bot_pin);
  ~HorizonLamp();
  void display(uint8_t* buf, uint16_t buf_len);
  void clear_display();

private:
  Adafruit_NeoPixel* top_strip;
  Adafruit_NeoPixel* bottom_strip;
  uint16_t strip_pixels;
  uint16_t frame_pixels;
};

#endif  // HORIZON_LAMP_H_
