#include "GvfDecoder.h"
#include "picojpeg.h"

#include "application.h"
#include "Config.h"

GvfDecoder::GvfDecoder(File gvf) {
  Load(gvf);
}

GvfDecoder::~GvfDecoder() {
  video.close();
}

bool GvfDecoder::Load(File gvf) {
  if (!gvf) return false;

  video = gvf;
  start_pos = video.position();
}

bool GvfDecoder::Reload() {
  video.seek(start_pos);
}

bool GvfDecoder::ReadFrame(uint8_t* buf, uint16_t buflen) {
  buflen = min(buflen, FRAME_BYTES);
  if (!video) return false;
  if (!video.available()) {
    Reload();
  }
  byte header = video.read();
  int br = video.read(buf, buflen);
  return (header == 0 && br == buflen);
}
