#ifndef JPEG_DECODER_H_
#define JPEG_DECODER_H_

#include "VideoStream.h"
#include "application.h"
#include "picojpeg.h"
#include "SD.h"
#include "Config.h"

#define IMG_BUF_SIZE   FRAME_BYTES*16

static unsigned char pjpeg_need_bytes_callback(
  unsigned char* buf, unsigned char buf_size, unsigned char *bytes_actually_read, void* this_pointer);

class JpegDecoder: public VideoStream {
 public:
   JpegDecoder(File jpeg_video);
  ~JpegDecoder();

  bool ReadFrame(uint8_t* buf, uint16_t buflen);

  // For pjpeg use only.
  unsigned char PjpegCallback(
    unsigned char* buf, unsigned char buf_size, unsigned char *bytes_actually_read);

 private:
   bool Load(File jpeg_video);
   bool Reload();
   bool DecodeNext();

   File image_file;
   uint8_t img_buf[IMG_BUF_SIZE];
   pjpeg_image_info_t image_info;
   int mcu_y = 0;
   int row_y = 0;
   bool rows_decoded = false;
};

#endif  // JPEG_DECODER_H_
