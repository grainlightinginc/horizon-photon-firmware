#include "Config.h"  // Defines HORIZON/ECLIPSE mode.

#include "SD.h"
#include "APDS-9960.h"

#include <memory>
#include "application.h"

#include "VideoPlayer.h"
#include "VideoStream.h"

#include "HorizonLamp.h"
#include "EclipseLamp.h"

SYSTEM_MODE(MANUAL);

typedef enum CmdOpcodeTag {
    CMD_DISP_FRAME = 0,
    CMD_DISP_BOT_FRAME,
    CMD_STREAM_MODE,
    CMD_DISCOVER,
    CMD_LOCAL_MODE,
    CMD_BR_UP,
    CMD_BR_DOWN,
    CMD_NEXT_VID,
    CMD_PREV_VID,
    CMD_SET_BR
} CmdOpcode;

SparkFun_APDS9960 apds = SparkFun_APDS9960();

struct State {
    volatile int apds_isr_flag = 0;

    unsigned int localPort = 43690;
    UDP udp;
#ifdef SD_CARD
    bool streaming = FALSE;
#else
    bool streaming = TRUE;
#endif
    bool booting = FALSE;
    bool connected = FALSE;

    uint32_t frame_count = 0;

    char device_name[64] = {0};

    VideoStream* video = NULL;
    VideoPlayer* player = NULL;
    Lamp* lamp;

    uint32_t streaming_start_time;
    uint32_t gesture_start_time;

    uint32_t last_connection_attempt = 0;
} State;


void apdsInterrupt() {
  if (digitalRead(APDS9960_intPin) == LOW) {
    if (!State.apds_isr_flag) {
      digitalWrite(D7, HIGH);
      State.gesture_start_time = millis();
      State.apds_isr_flag = 1;
    }
  }
}

void handler(const char *topic, const char *data) {
    strncpy(State.device_name, data, 64);
    Serial.println(String(data));
}

const char* GetIP() {
  IPAddress ip = WiFi.localIP();
  static char ip_str[16];
  sprintf(ip_str, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
  return ip_str;
}


void startStreaming() {
#ifdef SD_CARD
  State.streaming_start_time = millis();
  State.streaming = true;
#endif
}

void stopStreaming() {
  State.streaming = false;
}

bool streamingTimeout() {
  return State.streaming && ((millis() - State.streaming_start_time) > 5000);
}

void broadcastCommand(uint8_t c) {
  /*
  State.udp.beginPacket(IPAddress(255,255,255,255), State.localPort);
  State.udp.write(&c, 1);
  State.udp.endPacket();
  */
}

void handleGesture() {
  int res = apds.readGesture();
  detachInterrupt(APDS9960_intPin);
  State.apds_isr_flag = (res == APDS9960_CONTINUE || digitalRead(APDS9960_intPin) == LOW);
  if (!State.apds_isr_flag) digitalWrite(D7, LOW);
  attachInterrupt(APDS9960_intPin, apdsInterrupt, FALLING);
  if (res != APDS9960_CONTINUE) {
      uint32_t duration = millis() - State.gesture_start_time;
      //Spark.publish("duration", String(duration));
      //Spark.publish("gesture", String(res));
      switch ( res ) {
          case MAPPED_PREV_VID:
            State.video = State.player->prev_video(duration);
            broadcastCommand(CMD_PREV_VID);
            Serial.println(">> PREV");
            break;
          case MAPPED_NEXT_VID:
            State.video = State.player->next_video(duration);
            broadcastCommand(CMD_NEXT_VID);
            Serial.println(">> NEXT");
            break;
          case MAPPED_BR_DOWN:
            State.lamp->br_down(4096 / duration);
            broadcastCommand(CMD_BR_DOWN);
            Serial.println(">> BR_DOWN");
            break;
          case MAPPED_BR_UP:
            State.lamp->br_up(4096 / duration);
            broadcastCommand(CMD_BR_UP);
            Serial.println(">> BR_UP");
            break;
          default:
            break;
        }
    }
}

void setup() {
  pinMode(APDS9960_intPin, INPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(SD_chipSelect, OUTPUT);

  Serial.begin(9600);

  if ( apds.init() ) {
    Serial.println(F("APDS-9960 initialization complete"));
  } else {
    Serial.println(F("Something went wrong during APDS-9960 init!"));
  }
    // Start running the APDS-9960 gesture sensor engine
  if ( apds.enableGestureSensor(true) ) {
    Serial.println(F("Gesture sensor is now running"));
  } else {
    Serial.println(F("Something went wrong during gesture sensor init!"));
  }
  attachInterrupt(APDS9960_intPin, apdsInterrupt, FALLING);

  #if HORIZON
    State.lamp = new HorizonLamp(FRAME_PIXELS, TOP_STRIP_PIN, BOT_STRIP_PIN);
  #elif ECLIPSE
    State.lamp = new EclipseLamp(FRAME_PIXELS, STRIP_PIN);
  #endif
    State.lamp->clear_display();

  delay(2000);
  if (!SD.begin(SD_chipSelect)) {
    Serial.println("initialization failed!");
  } else {
    Serial.println("SD card init successful!");
    State.player = new VideoPlayer("/gvf", GVF);
    // Queue startup video.
    State.video = State.player->play_video("/startup.gvf");
    //State.video = load_video(SD.open("/startup.gvf"), GVF);
    if (State.video != NULL) {
      Serial.println("Playing startup video!");
      State.booting = TRUE;
    } else {
      Serial.println("Open startup failed!");
      State.booting = FALSE;
      State.video = State.player->next_video();
    }
  }

  if (WiFi.hasCredentials()) {
    WiFi.connect();
  }
}

void loop() {
    static unsigned char buffer[1024];
    if (Spark.connected()) Spark.process();

    if (WiFi.ready() && !Spark.connected()) {
      if (millis() - State.last_connection_attempt > 5000) {
        State.last_connection_attempt = millis();
        Spark.connect();
      }
    }

    if (State.apds_isr_flag == 1)  {
        handleGesture();
    }

    if (!State.connected && Spark.connected()) {
      Serial.println("Connected!");
      State.connected = TRUE;

      Spark.subscribe("spark/", handler);
      Spark.publish("spark/device/name");
      Spark.publish("ip", GetIP());
  #if HORIZON
      Spark.publish("lamp", "horizon");
  #elif ECLIPSE
      Spark.publish("lamp", "eclipse");
  #endif
      Spark.publish("video", State.player->video_name());

      State.udp.begin(State.localPort);
      return;
    }

    if (State.connected) {
      // Check if data has been received
      int packet_size = State.udp.parsePacket();
      if (packet_size > 0) {

          State.udp.read(buffer, 1024);
          State.udp.flush();

          IPAddress sender_ip = State.udp.remoteIP();
          if (sender_ip == WiFi.localIP()) return;
          int port = State.udp.remotePort();

          State.udp.beginPacket(sender_ip, port);

          switch (buffer[0]) {
              case CMD_DISCOVER: {
                  char discover_str[64];
                  sprintf(discover_str, "%s,%s", State.device_name, GetIP());

                  State.udp.write((byte*)discover_str, strlen(discover_str));
                  break;
              }
              case CMD_STREAM_MODE: {
                  startStreaming();
                  break;
              }
              case CMD_LOCAL_MODE: {
                  stopStreaming();
                  break;
              }
              case CMD_DISP_FRAME: {
                startStreaming();
                State.lamp->display(buffer + 1, min(packet_size - 1, FRAME_BYTES));
                State.udp.write((byte*)"f", 1);
                break;
              }
              case CMD_BR_UP: {
                  State.udp.write((byte*)"^", 1);
                  State.lamp->br_up();
                  break;
              }
              case CMD_BR_DOWN: {
                  State.udp.write((byte*)"v", 1);
                  State.lamp->br_down();
                  break;
              }
              case CMD_SET_BR: {
                  State.udp.write((byte*)"s", 1);
                  State.lamp->br_set(buffer[1]);
                  break;
              }
              case CMD_NEXT_VID: {
                  State.udp.write((byte*)">", 1);
                  State.video = State.player->next_video();
                  break;
              }
              case CMD_PREV_VID: {
                  State.udp.write((byte*)"<", 1);
                  State.video = State.player->prev_video();
                  break;
              }
              default: break;
          }

          State.udp.endPacket();
      } else if (streamingTimeout()) {
        stopStreaming();
      }
    }

    // Display a frame from the local video if we need to.
    if (!State.streaming) {
        if (State.booting && State.frame_count >= 72) {
          State.video = State.player->next_video();
          State.booting = false;
        }
        // Display the next frame from the video.
        if (State.video != NULL && State.video->ReadFrame(buffer + 1, 1023)) {
            State.lamp->display(buffer + 1, FRAME_BYTES);

            if (State.connected) {
              /*IPAddress echo(192,168,1,123);
              int port = 43690;

              State.udp.beginPacket(echo, port);
              buffer[0] = 0;
              State.udp.write(buffer, FRAME_BYTES + 1);
              State.udp.endPacket();*/
            }

            delay(14);
            State.frame_count++;
            if (State.frame_count % 300 == 0) Serial.println("Still alive!");
        } else {
            digitalWrite(D7, HIGH);
        }
    }
}
