#ifndef INTERPOLATOR_H_
#define INTERPOLATOR_H_

#include <memory>
#include "Config.h"
#include "VideoStream.h"

class Interpolator : public VideoStream {
public:
  Interpolator(VideoStream* video, uint8_t n_frames, uint8_t step_size = 1);
  bool ReadFrame(uint8_t* buf, uint16_t buflen);

private:
  byte frame_a[FRAME_BYTES];
  byte frame_b[FRAME_BYTES];
  byte* current_frame;
  byte* next_frame;

  uint8_t interpolation_frames;
  uint8_t interpolation_pos;
  uint8_t step_size;

  std::auto_ptr<VideoStream> video;
};

#endif  // INTERPOLATOR_H_
