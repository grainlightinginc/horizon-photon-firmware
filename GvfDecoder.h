#ifndef GVF_DECODER_H_
#define GVF_DECODER_H_

#include "VideoStream.h"
#include "application.h"
#include "picojpeg.h"
#include "SD.h"
#include "Config.h"

class GvfDecoder: public VideoStream {
 public:
   GvfDecoder(File gvf);
  ~GvfDecoder();

  bool ReadFrame(uint8_t* buf, uint16_t buflen);

 private:
   bool Load(File gvf);
   bool Reload();

   File video;
   uint32_t start_pos;
};

#endif  // GVF_DECODER_H_
