#ifndef ECLIPSE_LAMP_H_
#define ECLIPSE_LAMP_H_

#include "application.h"
#include "neopixel.h"
#include "Lamp.h"

class EclipseLamp : public Lamp {
public:
  EclipseLamp(uint16_t frame_pixels, uint8_t pin);
  ~EclipseLamp();
  void display(uint8_t* buf, uint16_t buf_len);
  void clear_display();

private:
  Adafruit_NeoPixel* strip;
  uint16_t strip_pixels;
  uint16_t frame_pixels;
};

#endif  // ECLIPSE_LAMP_H_
