#ifndef VIDEO_PLAYER_H_
#define VIDEO_PLAYER_H_

#include "SD.h"
#include "Video.h"
#include "VideoStream.h"

#define MAX_VIDEOS 32
#define MAX_VIDEO_NAME_LEN 32
#define DEFAULT_BLEND_DURATION 128

class VideoPlayer {
public:
  VideoPlayer(const char* dirname, FileType file_type);
  VideoStream* next_video(uint32_t blend_duration = DEFAULT_BLEND_DURATION);
  VideoStream* prev_video(uint32_t blend_duration = DEFAULT_BLEND_DURATION);
  VideoStream* play_video(const char* name, uint32_t blend_duration = DEFAULT_BLEND_DURATION);
  void start_video();
  void stop_video();
  const char* video_name() { return cur_video_name; };

private:
  void read_video_names(const char* dirname);
  char video_names[MAX_VIDEOS][MAX_VIDEO_NAME_LEN];
  int num_videos = 0;
  int cur_video = 0;
  char cur_video_name[MAX_VIDEO_NAME_LEN];

  FileType vid_type;
  VideoStream* video = NULL;
};

#endif  // VIDEO_PLAYER_H_
