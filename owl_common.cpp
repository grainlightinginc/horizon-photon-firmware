#include "owl_common.h"

int mod(int k, int n) {
 // From http://stackoverflow.com/questions/12276675/modulus-with-negative-numbers-in-c
 return ((k %= n) < 0) ? k+n : k;
}
