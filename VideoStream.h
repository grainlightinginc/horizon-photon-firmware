#ifndef VIDEO_STREAM_H_
#define VIDEO_STREAM_H_

#include "application.h"

class VideoStream {
public:
  virtual bool ReadFrame(uint8_t* buf, uint16_t buf_len) = 0;
  virtual ~VideoStream() {};
};

#endif  // VIDEO_STREAM_H_
