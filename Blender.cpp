#include "application.h"
#include "Blender.h"
#include "Config.h"

Blender::Blender(VideoStream* source_, VideoStream* target_, uint16_t n_frames, BlendType type) :
  source(source_), target(target_) {

  blend_pos = n_frames;  // This counts downward.
  blend_frames = n_frames;
  blend_type = type;
}

bool Blender::ReadFrame(uint8_t* buf, uint16_t buflen) {
  buflen = min(buflen, FRAME_BYTES);

  if (blend_pos == 0) {
    return target->ReadFrame(buf, buflen);
  } else {
    static byte frame[FRAME_BYTES];

    if (source.get() == NULL || !source.get()) {
      memset(buf, 0, buflen);
    } else if (!source->ReadFrame(buf, buflen)) {
      return false;
    }

    if (target.get() == NULL) {
      memset(frame, 0, buflen);
    } else if (!target->ReadFrame(frame, buflen)) {
      return false;
    }

    for (int i = 0; i < FRAME_BYTES; ++i) {
        uint8_t x = blend_frames;
        uint32_t start = buf[i];
        uint32_t end = frame[i];
        buf[i] = ((start * blend_pos) + end * (x - blend_pos + 1)) / (x + 1);
    }
    blend_pos--;
    if (blend_pos == 0) {
      // When blend period is over, delete the source stream.
      source.reset();
    }
    return true;
  }
}
