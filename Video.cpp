#include "Video.h"
#include "SD.h"
#include "JpegDecoder.h"
#include "GvfDecoder.h"
#include "Interpolator.h"

VideoStream* load_video(File f, FileType t) {
  VideoStream* s = NULL;
  uint8_t interpolation_frames = 0;
  uint8_t playback_speed = 100;

  switch (t) {
  case JPEG: {
    s = new JpegDecoder(f);
    break;
  }
  case GVF: {
    while (true) {
      uint8_t header = f.peek();
      if (header & 0x80) {
        header = f.read();
        uint8_t val = f.read();
        if (header == 0x87) {
          interpolation_frames = val;
        } else if (header == 0x88) {
          playback_speed = val;
        }
      } else {
        break;
      }
    }
    s = new GvfDecoder(f);
    break;
  }
  default: break;
  }
  if (s == NULL) return s;

  if (interpolation_frames != 0) {
    s = new Interpolator(s, interpolation_frames);
  } else if (playback_speed != 0 && playback_speed != 100) {
    s = new Interpolator(s, 99, playback_speed);
  }

  return s;
}
